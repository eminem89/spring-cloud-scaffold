package org.eminem.spring.cloud.scaffold.feign.controller;

import com.google.common.collect.Maps;
import org.eminem.spring.cloud.scaffold.feign.entity.SysUserEntity;
import org.eminem.spring.cloud.scaffold.feign.feign.UserFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Map;

@RestController
public class UserCustomerController {

    @Autowired
    private UserFeignClient userFeignClient;

    @GetMapping("/{id}")
    public SysUserEntity getUserById(@PathVariable Long id) {
        return this.userFeignClient.getUserById(id);
    }

    @GetMapping("/user/{id}")
    public SysUserEntity getUserByIdNew(@PathVariable Long id) {
        return this.userFeignClient.findUserById(id);
    }

    @GetMapping("/postuser/{id}")
    public SysUserEntity postUserById(@PathVariable Long id) {

        return this.userFeignClient.postUserById(id);
    }

    @GetMapping("/postentityuser/{id}")
    public SysUserEntity postEntityUserById(@PathVariable Long id) {
        SysUserEntity user = new SysUserEntity();
        user.setUserId(id);
        return this.userFeignClient.postUserByIdPo(user);
    }

    @GetMapping("/postmapuser/{id}")
    public SysUserEntity postMapUserById(@PathVariable Long id) {
       Map<String,Object> map = Maps.newHashMap();
        map.put("id","1");
        return this.userFeignClient.postUserByIdParam(map);
    }







}
