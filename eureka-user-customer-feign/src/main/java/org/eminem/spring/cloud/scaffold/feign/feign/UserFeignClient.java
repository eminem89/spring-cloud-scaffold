package org.eminem.spring.cloud.scaffold.feign.feign;

import org.eminem.spring.cloud.scaffold.feign.entity.SysUserEntity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@Component
@FeignClient(name="eureka-user")
public interface UserFeignClient {

    @GetMapping("/eureka-user/{id}")
    public SysUserEntity getUserById(@PathVariable("id") Long id);

    @RequestMapping(value="/eureka-user/{id}",method = RequestMethod.GET)
    public SysUserEntity findUserById(@PathVariable("id") Long id);

    @PostMapping("/eureka-user/getuser")
    public SysUserEntity postUserById(@RequestParam(name = "id",defaultValue="0") Long id);

    //直接传递javabean过来
    @PostMapping(value = "/eureka-user/getuserpo", consumes = "application/json",produces = "application/json")
    public SysUserEntity postUserByIdPo(@RequestBody SysUserEntity userEntity);

//    @RequestMapping(method = RequestMethod.POST, value = "/eureka-user/getuserpo", consumes = "application/json",produces = "application/json")
//    public SysUserEntity postUserByIdPo(@RequestBody SysUserEntity userEntity);
    //传递map过来
    @PostMapping("/eureka-user/getuserparam")
    public SysUserEntity postUserByIdParam(@RequestParam Map<String, Object> param);



}
