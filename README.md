# spring-cloud-scaffold

#### 项目介绍
spring-cloud-scaffold

#### 软件架构
软件架构说明

1.spring boot 2.0.1.RELEASE

2.spring cloud Finchley.M9

#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. Eureka服务注册与发现

按照官方文档搭建好注册中心和服务，为了使配置服务器有所作用，将服务端口放在配置服务器上的配置文件上。<br/>
主要是Eureka Server +Spring Security认证配置时出了问题，配置文件上：
```
1.X版本：

security:
  basic:
    enabled: true
  user:
    name:
    password: 

变成了：

spring:
  security:
    basic:
      enabled: true
    user:
      name:
      password: 

而且只要添加了Spring Security Starter依赖，spring.security.basic.enabled默认就是true，改成false也没用。
```
启动起来，客户端报异常：Cannot execute request on any known server。导致服务注册失败。<br/>
各种检查配置，折腾了几个小时也没搞好，最后在GitHub上找到了[答案](https://github.com/spring-cloud/spring-cloud-netflix/issues/2754)，原来是SpringBoot从2.0.0.RC1升级到2.0.0.RELEASE的时候，有个类SpringBootWebSecurityConfiguration发生了变化：<br/>
```
public class SpringBootWebSecurityConfiguration {

    @Configuration
    @Order(SecurityProperties.BASIC_AUTH_ORDER)
    static class DefaultConfigurerAdapter extends WebSecurityConfigurerAdapter {

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            super.configure(http);
            http.csrf().disable();
        }
    }
}

```
新建这个类放在Eureka Server项目里面就可以了。或者将SpringCloud降到Finchley.M6及以下，同时SpringBoot降级到2.0.0.RC1，只能说尝鲜需谨慎。。。<br/>
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)