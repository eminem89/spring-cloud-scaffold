package org.eminem.spring.cloud.scaffold.autheurekausercustomer.controller;

import org.eminem.spring.cloud.scaffold.autheurekausercustomer.entity.SysUserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class UserCustomerController {


    @Autowired
    private RestTemplate restTemplate;

    @GetMapping("/{id}")
    public SysUserEntity getUserById(@PathVariable Long id) {
        return this.restTemplate.getForObject("http://root:123456localhost:8074/auth-eureka-user/"+id,SysUserEntity.class);
    }

}
