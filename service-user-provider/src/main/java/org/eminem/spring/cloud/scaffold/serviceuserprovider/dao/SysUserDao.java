
package org.eminem.spring.cloud.scaffold.serviceuserprovider.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.eminem.spring.cloud.scaffold.serviceuserprovider.entity.SysUserEntity;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 系统用户
 * 
 */
@Component
public interface SysUserDao extends BaseMapper<SysUserEntity> {
	
	/**
	 * 自定义sql
	 * @param userId  用户ID
	 */
	public SysUserEntity queryCustomSql(Long userId);
	
}
