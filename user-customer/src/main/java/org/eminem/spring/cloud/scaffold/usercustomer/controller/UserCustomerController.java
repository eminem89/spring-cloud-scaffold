package org.eminem.spring.cloud.scaffold.usercustomer.controller;

import org.eminem.spring.cloud.scaffold.usercustomer.entity.SysUserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class UserCustomerController {


    @Autowired
    private RestTemplate restTemplate;

    @GetMapping("/{id}")
    public SysUserEntity getUserById(@PathVariable Long id) {
        return this.restTemplate.getForObject("http://localhost:8081/service-user-provider/"+id,SysUserEntity.class);
    }

}
