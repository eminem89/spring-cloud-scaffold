package org.eminem.spring.cloud.scaffold.eurekauser.controller;

import org.eminem.spring.cloud.scaffold.eurekauser.dao.SysUserDao;
import org.eminem.spring.cloud.scaffold.eurekauser.entity.SysUserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
public class SysUserController {

    @Autowired
    private SysUserDao sysUserDao;

    @GetMapping("/{id}")
    public SysUserEntity getUserById(@PathVariable Long id) {
        return this.sysUserDao.queryCustomSql(id);
    }

    //当写了@RequestParam时，是默认必需有默认值，如果没有报错
    //http://localhost:8084/eureka-user/getuser?id=1
    @PostMapping("/getuser")
    public SysUserEntity postUserById(@RequestParam(name = "id",defaultValue="0") Long id){
        return this.sysUserDao.queryCustomSql(id);
    }

    //直接传递javabean过来
    @PostMapping(value = "/getuserpo",consumes = "application/json",produces = "application/json")
    public SysUserEntity postUserByIdPo(@RequestBody SysUserEntity userEntity){
        return this.sysUserDao.queryCustomSql(userEntity.getUserId());
    }

    //传递map过来
    @PostMapping("/getuserparam")
    public SysUserEntity postUserByIdParam(@RequestParam Map<String, Object> param){
        return this.sysUserDao.queryCustomSql(Long.parseLong(param.get("id").toString()));
    }
}
