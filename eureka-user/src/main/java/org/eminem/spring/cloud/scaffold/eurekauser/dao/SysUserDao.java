
package org.eminem.spring.cloud.scaffold.eurekauser.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.eminem.spring.cloud.scaffold.eurekauser.entity.SysUserEntity;
import org.springframework.stereotype.Component;

/**
 * 系统用户
 * 
 */
@Component
public interface SysUserDao extends BaseMapper<SysUserEntity> {
	
	/**
	 * 自定义sql
	 * @param userId  用户ID
	 */
	public SysUserEntity queryCustomSql(Long userId);
	
}
