package org.eminem.spring.cloud.scaffold.eurekausercustomer.controller;

import org.eminem.spring.cloud.scaffold.eurekausercustomer.entity.SysUserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController
public class UserCustomerController {


    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private DiscoveryClient discoveryClient;

    @GetMapping("/{id}")
    public SysUserEntity getUserById(@PathVariable Long id) {
        return this.restTemplate.getForObject("http://localhost:8084/eureka-user/"+id,SysUserEntity.class);
    }

    @GetMapping("/user-instance")
    public List<ServiceInstance> showInfo(){
        return this.discoveryClient.getInstances("eureka-user");
    }





}
