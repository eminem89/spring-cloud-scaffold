package org.eminem.spring.cloud.scaffold.autheurekauser.controller;

import org.eminem.spring.cloud.scaffold.autheurekauser.dao.SysUserDao;
import org.eminem.spring.cloud.scaffold.autheurekauser.entity.SysUserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SysUserController {

    @Autowired
    private SysUserDao sysUserDao;

    @GetMapping("/{id}")
    public SysUserEntity getUserById(@PathVariable Long id) {
        return this.sysUserDao.queryCustomSql(id);
    }
}
